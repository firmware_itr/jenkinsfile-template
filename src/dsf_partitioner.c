/**
 * @file       dsf_partitioner.c
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      DSF Partitioner - Manage partition's operations
 * @note       None
 */
/* Public includes ---------------------------------------------------------- */
#include "dsf_partitioner.h"


/* Private includes --------------------------------------------------------- */
/* Private defines ---------------------------------------------------------- */
#define DSF_MAX_PARTITION         20
#define DSF_MAX_JOURNAL_RECORD    20

/* Private enumerate/structure ---------------------------------------------- */
typedef dsf_partition_config_t dsf_partition_record_t;

typedef struct
{
  uint16_t version:
  uint32_t partition_count;
  dsf_partition_record_t records[DSF_MAX_PARTITION];
  uint16_t crc;
}
dsf_partition_metadata_t;

typedef struct
{
  uint16_t version;
  uint32_t record_count;
  uint16_t sync_index;
  uint16_t crc;
}
dsf_journal_metadata_t;

typedef struct
{
  uint32_t record_size[DSF_MAX_JOURNAL_RECORD];
  uint16_t crc;
}
dsf_journal_record_table_t;

/* Private macros ----------------------------------------------------------- */
/* Public variables --------------------------------------------------------- */
/* Private variables -------------------------------------------------------- */
static dsf_journal_metadata_t mjm;  // Main Journal Metadata
static dsf_journal_metadata_t tjm;  // Temporary Journal Metadata

/* Private prototypes ------------------------------------------------------- */
static int dsf_journal_init(void);
static int dsf_journal_write(partition_id_t id, size_t offset, uint8_t *buffer, uint32_t length);
static int dsf_journal_sync(void);

/* Public implementations --------------------------------------------------- */


/* Private implementations -------------------------------------------------- */


/* End of file -------------------------------------------------------------- */
