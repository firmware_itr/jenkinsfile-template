/**
 * @file       dsf_logger.c
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      
 * @note       None
 */
/* Public includes ---------------------------------------------------------- */
#include "dsf_logger.h"
#include "dsf_partitioner.h"

/* Private includes --------------------------------------------------------- */
/* Private defines ---------------------------------------------------------- */
/* Private enumerate/structure ---------------------------------------------- */
typedef struct
{
	uint16_t version;
	uint32_t partition_id;
	uint32_t data_size;
	uint32_t capacity;
	uint32_t reader;
	uint32_t writer;
	uint8_t  crc;
}
dsf_logger_t;

/* Private macros ----------------------------------------------------------- */
/* Public variables --------------------------------------------------------- */
/* Private variables -------------------------------------------------------- */
/* Private prototypes ------------------------------------------------------- */
/* Public implementations --------------------------------------------------- */


/* Private implementations -------------------------------------------------- */


/* End of file -------------------------------------------------------------- */
