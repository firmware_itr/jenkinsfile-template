/**
 * @file       dsf_partitioner.h
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      DSF Partitioner - Manage partition's operations
 * @note       None
 * @example    example_partitioner.c
 */

/* Define to prevent recursive inclusion ------------------------------------ */
#ifndef __DSF_PARTITION_H
#define __DSF_PARTITION_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ----------------------------------------------------------------- */
#include <stdint.h>

/* Public defines ----------------------------------------------------------- */
/* Public enumerate/structure ----------------------------------------------- */
/**
 * @brief  Partition return error codes
 */
typedef enum
{
	DSF_PARTITION_OK,
	DSF_PARTITION_ERR
}
dsf_partition_err_t;

/**
 * @brief Unique number used to identify Partition in Partition Table
 */
typedef uint32_t dsf_partition_id_t;

/**
 * @brief Partition's initial configrations
 */
typedef struct
{
	dsf_partition_id_t  id;
	uint32_t            base_address;
	size_t              size;
}
dsf_partition_config_t;

/* Public macros ------------------------------------------------------------ */
/* Public variables --------------------------------------------------------- */
/* Public APIs -------------------------------------------------------------- */
/**
 * @brief  Initialize partitioner
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_partition_init(void);

/**
 * @brief  Wipe out all partitions' metadata
 * 
 * @param  None
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_partition_format(void);

/**
 * @brief  Create a partition
 * 
 * @param[in]  <config>    Partition's configurations
 * @param[in]  <count>     Number of partition configurations
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_partition_create(dsf_partition_config_t *config, uint32_t count);

/**
 * @brief  Read data from partition
 * 
 * @param[in]  <id>        Partition ID
 * @param[in]  <offset>    Address where the data should be read, relative to the beginning of the partition.
 *                         Range: [0, partition_size - 1]
 * @param[out] <buffer>    Buffer storing read data
 * @param[in]  <length>    Length of read data, in bytes.
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_partition_read(partition_id_t id, size_t offset, uint8_t *buffer, size_t length);

/**
 * @brief  Write data to partition
 * 
 * @param[in]  <id>        Partition ID
 * @param[in]  <offset>    Address where the data should be written, relative to the beginning of the partition
 * @param[in]  <buffer>    Buffer storing written data
 * @param[in]  <length>    Length of written data, in bytes.
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_partition_write(partition_id_t id, size_t offset, uint8_t *buffer, uint32_t length);

/**
 * @brief  Start logging write operations in the journal for the specified partition.
 * 
 * @param  None
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_journal_start(void);

/**
 * @brief  Stop journaling. Copy data from journal partition to destination partition
 * 
 * @param  None
 * 
 * @return dsf_partition_err_t 
 */
dsf_partition_err_t dsf_journal_stop(void);

/* -------------------------------------------------------------------------- */
#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* __DSF_PARTITION_H */

/* End of file -------------------------------------------------------------- */
