/**
 * @file       dsf_logger.h
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      DSF Data Logger
 * @note       None
 * @example    example_logger.c
 */
/* Define to prevent recursive inclusion ------------------------------------ */
#ifndef __DSF_LOGGER_H
#define __DSF_LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ----------------------------------------------------------------- */
#include <stdint.h>

/* Public defines ----------------------------------------------------------- */
/* Public enumerate/structure ----------------------------------------------- */
/**
 * @brief 
 */
typedef enum
{
	DSF_LOGGER_OK,
	DSF_LOGGER_ERR
}
dsf_logger_err_t;

/**
 * @brief 
 */
typedef uint32_t dsf_logger_id_t;

/**
 * @brief 
 */
typedef struct
{
	dsf_logger_id_t  id;
	size_t           data_size;
	size_t           capacity;
}
dsf_logger_config_t;

/* Public macros ------------------------------------------------------------ */
/* Public variables --------------------------------------------------------- */
/* Public APIs -------------------------------------------------------------- */
/**
 * @brief  Create a logger
 * 
 * @param[in]  <config>    Logger's configurations
 * @param[in]  <count>     Number of logger configurations
 * 
 * @return dsf_logger_err_t 
 */
dsf_logger_err_t dsf_logger_create(dsf_logger_config_t *config, size_t count);

/**
 * @brief Reset logger to initial state
 * 
 * @param[in]  <logger>  Logger ID
 * 
 * @return dsf_logger_err_t 
 */
dsf_logger_err_t dsf_logger_reset(dsf_logger_id_t id);

/**
 * @brief Write data to logger
 * 
 * @param[in]  <logger>  Logger ID
 * @param[in]  <w_buf>   Buffer contains written data
 * @param[in]  <count>   Number of written items
 * 
 * @note This API only write data to MCU SRAM. Use dsf_logger_sync() to sync data with external memory.
 * 
 * @return dsf_logger_err_t 
 */
dsf_logger_err_t dsf_logger_write(dsf_logger_id_t id, void *w_buf, size_t count);

/**
 * @brief Read data from logger
 * 
 * @param[in]   <logger>  Logger ID
 * @param[out]  <r_buf>   Buffer storing read data
 * @param[in]   <count>   Number of read items
 * 
 * @note This API only read data to MCU SRAM. Use dsf_logger_sync() to sync data with external memory.
 * 
 * @return dsf_logger_err_t 
 */
dsf_logger_err_t dsf_logger_read(dsf_logger_id_t id, void *r_buf, size_t count);

/**
 * @brief Sync current logger status to external memory
 * 
 * @param[in]  <logger>  Logger ID
 * 
 * @return dsf_logger_err_t 
 */
dsf_logger_err_t dsf_logger_sync(dsf_logger_id_t id);

/**
 * @brief Get the number of unread items in a logger
 * 
 * @param[in]   <logger>  Logger ID
 * @param[out]  <count>   Number of unread items
 * 
 * @return dsf_logger_err_t 
 */
dsf_logger_err_t dsf_logger_count(dsf_logger_id_t id, size_t *count);

/* -------------------------------------------------------------------------- */
#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* __DSF_LOGGER_H */

/* End of file -------------------------------------------------------------- */
