/**
 * @file       dsf_block_device.c
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      
 * @note       None
 */
/* Public includes ---------------------------------------------------------- */
#include "dsf_block_device.h"

/* Private includes --------------------------------------------------------- */
/* Private defines ---------------------------------------------------------- */
/* Private enumerate/structure ---------------------------------------------- */
/* Private macros ----------------------------------------------------------- */
/* Public variables --------------------------------------------------------- */
/* Private variables -------------------------------------------------------- */
static dsf_block_device_t m_dev = { NULL };

/* Private prototypes ------------------------------------------------------- */
/* Public implementations --------------------------------------------------- */
void dsf_bd_create(dsf_block_device_t *dev)
{
	m_dev = *dev;
}

dsf_bd_err_t dsf_bd_init(void)
{
	return m_dev.init();
}

dsf_bd_err_t dsf_bd_deinit(void)
{
	return m_dev.deinit();
}

dsf_bd_err_t dsf_bd_read(uint32_t address, uint8_t *buffer, size_t length)
{
	return m_dev.read(address, buffer, length);
}

dsf_bd_err_t dsf_bd_write(uint32_t address, uint8_t *buffer, size_t length)
{
	return m_dev.write(address, buffer, length);
}

size_t dsf_bd_get_size(void)
{
	return m_dev.size();
}

/* Private implementations -------------------------------------------------- */


/* End of file -------------------------------------------------------------- */
