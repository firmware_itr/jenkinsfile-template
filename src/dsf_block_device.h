/**
 * @file       dsf_block_device.h
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      DSF Block Device - An abstraction layer to communicate with memory devices
 * @note       None
 */
/* Define to prevent recursive inclusion ------------------------------------ */
#ifndef __DSF_BLOCK_DEVICE_H
#define __DSF_BLOCK_DEVICE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ----------------------------------------------------------------- */
#include <stdint.h>

/* Public defines ----------------------------------------------------------- */
/* Public enumerate/structure ----------------------------------------------- */
typedef enum
{
  DSF_BD_OK,
  DSF_BD_ERR
}
dsf_bd_err_t;

typedef struct
{
	dsf_bd_err_t (*init)(void);
	dsf_bd_err_t (*deinit)(void);
	dsf_bd_err_t (*read)(uint32_t address, uint8_t *buffer, size_t length);
	dsf_bd_err_t (*write)(uint32_t address, uint8_t *buffer, size_t length);
	size_t (*size)(void);
}
dsf_block_device_t;

/* Public macros ------------------------------------------------------------ */
/* Public variables --------------------------------------------------------- */
/* Public APIs -------------------------------------------------------------- */
/**
 * @brief Create new memory device
 * 
 * @return None
 */
void dsf_bd_create(dsf_block_device_t *dev);

/**
 * @brief Initialize memory device
 * 
 * @return dsf_bd_err_t 
 */
dsf_bd_err_t dsf_bd_init(void);

/**
 * @brief Deinitialize memory Device
 * 
 * @return dsf_bd_err_t 
 */
dsf_bd_err_t dsf_bd_deinit(void);

/**
 * @brief  Read data from memory device
 * 
 * @param[in]   <address>  Address on memory device, where data are read from
 * @param[out]  <buffer>   Buffer storing read data
 * @param[in]   <length>   Length of read data, in bytes
 * 
 * @return dsf_bd_err_t 
 */
dsf_bd_err_t dsf_bd_read(uint32_t address, uint8_t *buffer, size_t length);

/**
 * @brief  Write data to memory device
 * 
 * @param[in]  <address>  Address on memory device, where data are written to
 * @param[in]  <buffer>   Buffer storing written data
 * @param[in]  <length>   Length of written data, in bytes
 * 
 * @return dsf_bd_err_t 
 */
dsf_bd_err_t dsf_bd_write(uint32_t address, uint8_t *buffer, size_t length);

/**
 * @brief  Get the total size of the underlying memory device
 * 
 * @return  The total size of the underlying memory device in bytes
 */
size_t dsf_bd_get_size(void);

/* -------------------------------------------------------------------------- */
#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* __DSF_BLOCK_DEVICE_H */

/* End of file -------------------------------------------------------------- */
