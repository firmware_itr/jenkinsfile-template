# Data Storage Framework (DSF)

***v1.0.0***

---

**Table of Contents**

[TOC]

---

## Overview

The Data Storage Framework (DSF) is a collection of high-level abstraction layers used to interact with the underlying memory devices on XII Implant Device.

List of framework features:

- **FRAM drivers** to communicate with FRAM chips at register level
- A **Block Device** layer to access raw contents on memory device, this is wrap layer for FRAM drivers
- A **Partitioner** layer to controls the memory layout, read/write data with built-in journaling for fail-safe functionality
- A **Logger** layer to records events and device logs in circular buffer format

**Notes:**

- *As of version **1.0.x**, the DSF is designed and optimized particularly for **FRAM** memory devices. If other memory types are involved, the framework will need to be modified.*

---
**Journaling**

Journaling is a fail-safe mechanism that protect multiple write operations against unexpected power outages. Basically, after a **journal_start()** is requested, all further write operations are buffered on a FRAM memory region and the data is only written back to the destination memory when the **journal_stop()** is invoked.

---

## FRAM Memory Layout

### Overview

- All user's data are stored on memory units called the **partitions **and the layer that handle partition operations (init/read/write,...) is the **Partitioner**
- The **Partition Metadata** contains information used to locate the partition on the FRAM memory
- The number of journal records (**Record Count**) and the current syncing journal record position (**Sync Index**) are stored in the **Journal Metadata** to assist the **Partitioner** in determining where to begin syncing in the event of a power outage.
- The **Journal Record Table** stores the information used to locate the **Journal Records' positions** on the FRAM memory
- Each **Journal Record** contains information of a [partition write](#DSF_Partition_Write) operation
- **Byte order:** ***little-endian***

<img src="images/mem_layout/DSF_Memory_Layout.png" alt="DSF_Memory_Layout.png" width=60% />

### Partition Metadata



![DSF_Partition_Metadata.png](images/mem_layout/DSF_Partition_Metadata.png)

- Base address: **0x00000000** (located at the beginning of the FRAM chip)
- **Field descriptions:**
    - **Version**: the version of partition metadata. 0x0100 <--> v1.0
    - **Partition Count**: total number of partitions on memory
    - **ID**: a **unique** number that is used to distinguish the partitions
    - **Base Address**: the stating memory address of partition on the memory chip
    - **Size**: partition size in **bytes**
    - **CRC**: CRC-16/XMODEM polynomial 0x1021

### Journal Metadata

![DSF_Journal_Metadata_Main.png](images/mem_layout/DSF_Journal_Metadata_Main.png)

![DSF_Journal_Metadata_Temporary.png](images/mem_layout/DSF_Journal_Metadata_Temporary.png)

- There are two types of metadata areas in a journal: **Main** and **Temporary**. Only the Temporary region is updated during the sync process from Journal Records to the destination partition, and the content of the Temporary is copied to the Main when the sync process is completed.
- **Field Description:**
    - **Version**: the version of partition metadata. 0x0100 <--> v1.0
    - **Record Count**: total number of journal records on memory
    - **Sync Index**: current syncing record position
    - **CRC**: CRC-16/XMODEM polynomial 0x1021

### Journal Record Table

![DSF_Journal_Record_Table.png](images/mem_layout/DSF_Journal_Record_Table.png)

- Since the starting address of the Journal Records area is predefined, only the record sizes are needed to calculate the Journal Records' base address. 

- **Field Description:**
    - **Record Size**: journal record size in **bytes**
    - **CRC**: CRC-16/XMODEM polynomial 0x1021

### Journal Record

![DSF_Journal_Record.png](images/mem_layout/DSF_Journal_Record.png)

- **Field Description:**
    - **Partition ID**: ID of the partition in the [Partition Table](#Partition Metadata)
    - **Address**: the starting address where the **Data** is written to
    - **Data Size**: size of the **Data** in **bytes**
    - **Data**: the writing data
    - **CRC**: CRC-16/XMODEM polynomial 0x1021

### Logger

![DSF_Logger.png](images/mem_layout/DSF_Logger.png)

- **Field Description:**
    - **Version**: the version of partition metadata. 0x0100 <--> v1.0
    - **Partition ID**: ID of the partition in the [Partition Table](#Partition Metadata)
    - **Data Size**: size of each data in **bytes**
    - **Capacity**: the maximum number of elements in the logger
    - **Read Pointer**: used to determine the logger's current read position
    - **Write Pointer**: used to determine the logger's current read position
    - **Header CRC**: CRC value of the **Header only**. CRC-16/XMODEM polynomial 0x1021

## API

### API Layers

<img src="images/DSF_API_Layer.png" alt="DSF_API_Layer.png" width=90% />

### Workflow

#### Partitioner

##### DSF_Partition_Init

**Internal State**

<img src="images/api/partitioner/DSF_Partition_Init.png" alt="DSF_Partition_Init.png" width=80% />

<div style="page-break-after: always; break-after: page;"></div>

**Layer Interaction**

![DSF_Journal_Record_Table.png](UML/out/dsf_partitioner_init.png)

##### DSF_Partition_Format

**Internal State**

<img src="images/api/partitioner/DSF_Partition_Format.png" alt="DSF_Partition_Format.png" width=35% />

**Layer Interaction**

![dsf_partitioner_format.png](UML/out/dsf_partitioner_format.png)

##### DSF_Partition_Create

**Internal State**

<img src="images/api/partitioner/DSF_Partition_Create.png" alt="DSF_Partition_Create.png" width=35% />

**Layer Interaction**

![dsf_partitioner_create.png](UML/out/dsf_partitioner_create.png)

##### DSF_Partition_Read

**Internal State**

<img src="images/api/partitioner/DSF_Partition_Read.png" alt="DSF_Partition_Read.png" width=35% />

**Layer Interaction**

![dsf_partitioner_read.png](UML/out/dsf_partitioner_read.png)

##### DSF_Partition_Write

**Internal State**

<img src="images/api/partitioner/DSF_Partition_Write.png" alt="DSF_Partition_Write.png" width=80% />

**Layer Interaction**

**Write without Journaling**

![dsf_partitioner_write.png](UML/out/dsf_partitioner_write.png)

**Write with Journaling**

![dsf_partitioner_write_with_journal.png](UML/out/dsf_partitioner_write_with_journal.png)

#### Journaling

##### DSF_Journal_Start

<img src="images/api/journal/DSF_Journal_Start.png" alt="DSF_Journal_Start.png" width=35% />

##### DSF_Journal_Stop

<img src="images/api/journal/DSF_Journal_Stop.png" alt="DSF_Journal_Stop.png" width=50% />

##### Internal APIs

###### DSF_Journal_Init

<img src="images/api/journal/DSF_Journal_Init.png" alt="DSF_Journal_Init.png" width=100% />

###### DSF_Journal_Write

<img src="images/api/journal/DSF_Journal_Write.png" alt="DSF_Journal_Write.png" width=70% />

###### DSF_Journal_Sync

<img src="images/api/journal/DSF_Journal_Sync.png" alt="DSF_Journal_Sync.png" width=50% />

#### Logger

##### DSF_Logger_Create

**Internal State**

<img src="images/api/logger/DSF_Logger_Create.png" alt="DSF_Logger_Create.png" width=70% />

**Layer Interaction**

![dsf_logger_create.png](UML/out/dsf_logger_create.png)

##### DSF_Logger_Reset

**Internal State**

<img src="images/api/logger/DSF_Logger_Reset.png" alt="DSF_Logger_Reset.png" width=35% />

**Layer Interaction**

![dsf_logger_reset.png](UML/out/dsf_logger_reset.png)

##### DSF_Logger_Write

**Internal State**

<img src="images/api/logger/DSF_Logger_Write.png" alt="DSF_Logger_Write.png" width=35% />

**Layer Interaction**

![dsf_logger_write.png](UML/out/dsf_logger_write.png)

##### DSF_Logger_Read

**Internal State**

<img src="images/api/logger/DSF_Logger_Read.png" alt="DSF_Logger_Read.png" width=35% />

**Layer Interaction**

![dsf_logger_read.png](UML/out/dsf_logger_read.png)

##### DSF_Logger_Count

<img src="images/api/logger/DSF_Logger_Count.png" alt="DSF_Logger_Count.png" width=35% />

**Layer Interaction**

![dsf_logger_count.png](UML/out/dsf_logger_count.png)


##### DSF_Logger_Sync

<img src="images/api/logger/DSF_Logger_Sync.png" alt="DSF_Logger_Sync.png" width=35% />

### API References

For more details on DSF APIs, please navigate to generated Doxygen document in [PDF format](https://git.xiimedical.com/xiimedical/vytasom-common-mcu/src/branch/feature/data-storage-framework/dsf/doc/Doxygen/latex/refman.pdf) or [HTML format](Doxygen/html/index.html):

- [List of API files](Doxygen/html/files.html)
- [List of Functions](Doxygen/html/globals_func.html)
- [Examples](Doxygen/html/examples.html)
