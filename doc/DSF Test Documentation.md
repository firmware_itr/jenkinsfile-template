# DSF Test Documentation

***v1.0.0***

---

[TOC]

---

## Overview

Reasoning for choosing [ThrowTheSwitch Test Framework](http://www.throwtheswitch.org):
- Provide robust tools:
  - [Unity](http://www.throwtheswitch.org/unity): powerful Unit Testing in C for C
  - [CMock](http://www.throwtheswitch.org/cmock): automatically generate stubs and mocks for Unity Tests
  - [Ceedling](http://www.throwtheswitch.org/ceedling): test build management

- Free and open-source
- Ceedling can export code coverage reports using the gcovr plugin
- Ceedling is a command line tool so it can be integrate into CI/CD workflow

## Test cases

- All test cases are located in the **.../dsf/test** folder
- **Test files** MUST be prefixed with **test_**
- **Mock files** MUST be prefixed with **mock_**
- **Test cases** MUST be prefixed with **test_**
- Test template

```c
#include "unity.h"   // Unit test library, provides helper 
#include "bd_ram.h"  // Tested component

/* Test Setup ----------------------------------------------------------- */
void setUp(void)
{
}

void tearDown(void)
{
}

/* Test Cases ----------------------------------------------------------- */
void test_bd_ram_get_size(void)
{
	TEST_ASSERT_EQUAL(BD_RAM_SIZE, bd_ram_get_size());
}

void test_bd_ram_rw(void)
{
	uint32_t address = 0;
	static uint8_t w_buf[BD_RAM_SIZE];
	static uint8_t r_buf[BD_RAM_SIZE] = { 0 };

	memset(w_buf, 0xA5, sizeof(w_buf));

	TEST_ASSERT_EQUAL(0, bd_ram_write(address, w_buf, sizeof(w_buf)));
	TEST_ASSERT_EQUAL(0, bd_ram_read(address, r_buf, sizeof(r_buf)));
	TEST_ASSERT_EQUAL(0, memcmp(w_buf, r_buf, sizeof(w_buf)));
}
```

## Integrate Ceedling with VSCode

### Installation

**Required tools:**

- [Visual Studio Code (VSCode)](https://code.visualstudio.com/)
- [Ruby](https://www.ruby-lang.org/en/) (required by Ceedling)
- [Python3](https://www.python.org/) (required by Gcovr)
- [Ceedling](https://github.com/ThrowTheSwitch/Ceedling)
- [gcovr](https://github.com/gcovr/gcovr) (Code coverage tool)

**Installation steps:**

- Install Ruby and add Ruby to OS Path
- Install Python3 and add Python to OS Path
- Open terminal and run the following commands:

  ```shell
  gem install ceedling
  pip install gcovr
  ```

- Check if we have ceedling and gcovr installed:

  ```shell
  ceedling version
  gcovr --version
  ```

- Install VSCode
- Open VSCode and install [Ceedling Test Explorer](https://marketplace.visualstudio.com/items?itemName=numaru.vscode-ceedling-test-adapter) extension

### How to use

- Open folder **.../vytasom-common-mcu/dsf** in VSCode

- Click on the Ceedling Test Explorer icon ![image-20211210152701180](images/test/CTE_icon.png)

	![Ceedling_Test_Explorer.png](images/test/CTE.png)

- Figure descriptions:

  - **1**: Ceedling Test Explorer icon

  - **2**: List of test cases

  - **3**: Test title bar

        - ![CTE_Run_Icon.png](images/test/CTE_Run_Icon.png): Run all tests

        - ![CTE_Reload_Icon.png](images/test/CTE_Reload_Icon.png): Reload tests, used when new tests are added

  - **4**: Expanded context menu of the Ceedling Test Explorer

      - Enable/disable autorun: setup to re-run tests automatically whenever a file changes
      - Reset test states: reset tests to untested state
      - Tests Sorting options

- Individual test cases can be run by hovering the mouse over them and clicking the ![CTE_Run_Icon.png](images/test/CTE_Run_Icon.png)

	![CTE_Run_Icon.png](images/test/CTE_Hover.png)

- To generate **code coverage report**: open the **Command Palette** (**F1** or **Ctrl + Shift + P**) --> choose **Tasks: Run Task** --> choose **gcov**. The generated reports are located at: **.../dsf/ceedling/build/artifacts/gcov**

## Setup debug tests with CMake

**Required tools:**

- [CMake](https://cmake.org/)
- Windows OS: [Visual Studio Community](https://visualstudio.microsoft.com/)
- Linux/Mac: [Ninja](https://github.com/ninja-build/ninja)/Makefile with gcc

**Installation steps:**

- Install CMake and add CMake to OS path
- For Windows, install Visual Studio Community
- For Linux, install gcc
- Open VSCode and install extensions:

  - [CMake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
  - [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)

### How to use

- Open folder **.../vytasom-common-mcu/dsf** in VSCode
- Open **Ceedling Test Explorer** and click **Run all tests** to generate test runners and mock files for test cases
- Open the **Command Palette** --> choose **CMake: Select a Kit** --> Choose your build system (E.g., Visual Studio Community)
- Open the **Command Palette** --> choose **CMake: Configure**
- After running CMake: Configure, the projects files will be generated to **.../dsf/build** folder. Now open the project file (E.g., **dsf_test.sln**) and start debugging.

## Jenkins

