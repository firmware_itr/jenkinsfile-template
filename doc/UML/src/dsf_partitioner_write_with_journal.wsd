@startuml
participant dsf_sys as "dsf_sys"
participant dsf_partitioner as "dsf_partitioner"
participant FRAM as "FRAM"
== Start Journal ==
dsf_sys -> dsf_partitioner: call dsf_journal_start
alt Journal_Start flag is false
dsf_partitioner->dsf_partitioner: Set flag **Journal_Start** = true
dsf_partitioner->dsf_partitioner: Initialize **Record Table**, **Journal Metadata** on SRAM
dsf_partitioner -> dsf_sys: return DSF_PARTITION_OK
else Journal_Start flag is true
dsf_partitioner -> dsf_sys: return DSF_PARTITION_ERR
end
== Write Data to Journal ==
group call dsf_partition_write
dsf_sys -> dsf_partitioner: call dsf_partition_write with\n- **id**\n- **offset**\n- **buffer**\n- **length** 
alt id is correct
dsf_partitioner->dsf_partitioner: get **Partitioner address** from **id**
dsf_partitioner->dsf_partitioner: calculate **Destination Address**\nfrom **Partitioner address** and **offset**
alt Destination Address on Partitioner is correct
dsf_partitioner->dsf_partitioner: calculate **Journal Record Address**
alt Journal Record has enough space
dsf_partitioner -> FRAM:Write data from **buffer**\nto **Journal Record Address** with **length** bytes
dsf_partitioner -> dsf_partitioner: Update **Record Table**, **Journal Metadata** on SRAM
dsf_partitioner -> dsf_sys: return DSF_PARTITION_OK
else Journal Record is full
dsf_partitioner -> dsf_sys: return DSF_PARTITION_ERR
end
else Destination Address on Partitioner is incorrect
dsf_partitioner -> dsf_sys: return DSF_PARTITION_ERR
end
else id is incorrect
dsf_partitioner -> dsf_sys: return DSF_PARTITION_ERR
end
end group 
... call dsf_partition_write with n times ...
group call dsf_partition_write n
dsf_sys -> dsf_partitioner:
dsf_partitioner -> FRAM:
dsf_partitioner -> dsf_sys:
end group
== Stop Journal ==
dsf_sys -> dsf_partitioner: call dsf_journal_stop
alt Journal_Start flag is true
dsf_partitioner->FRAM: Update **Record Table**, **Temporary Journal Metadata**\nand **Main Journal Metadata** from SRAM to FRAM
group call dsf_journal_sync
dsf_partitioner->FRAM: Write data from log record to destination partitions
dsf_partitioner->FRAM: Update **Temporary Journal Metadata**
end group
dsf_partitioner->FRAM: Update **Main Journal Metadata**
dsf_partitioner->dsf_partitioner: Set flag **Journal_Start** = false
dsf_partitioner -> dsf_sys: return DSF_PARTITION_OK
else Journal_Start flag is false
dsf_partitioner -> dsf_sys: return DSF_PARTITION_ERR
end

@enduml