\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Data Structure Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Data Structures}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}Data Structure Documentation}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}journal\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}metadata\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{5}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Field Documentation}{5}{subsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.2.1}crc}{5}{subsubsection.3.1.2.1}%
\contentsline {subsubsection}{\numberline {3.1.2.2}record\_count}{5}{subsubsection.3.1.2.2}%
\contentsline {subsubsection}{\numberline {3.1.2.3}sync\_index}{6}{subsubsection.3.1.2.3}%
\contentsline {subsubsection}{\numberline {3.1.2.4}version}{6}{subsubsection.3.1.2.4}%
\contentsline {section}{\numberline {3.2}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}journal\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}record\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}table\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{6}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{6}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Field Documentation}{6}{subsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.1}crc}{6}{subsubsection.3.2.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2.2}record\_size}{7}{subsubsection.3.2.2.2}%
\contentsline {section}{\numberline {3.3}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{7}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{7}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Field Documentation}{7}{subsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.2.1}capacity}{7}{subsubsection.3.3.2.1}%
\contentsline {subsubsection}{\numberline {3.3.2.2}data\_size}{7}{subsubsection.3.3.2.2}%
\contentsline {subsubsection}{\numberline {3.3.2.3}id}{8}{subsubsection.3.3.2.3}%
\contentsline {section}{\numberline {3.4}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{8}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Detailed Description}{8}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Field Documentation}{8}{subsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.2.1}capacity}{8}{subsubsection.3.4.2.1}%
\contentsline {subsubsection}{\numberline {3.4.2.2}crc}{8}{subsubsection.3.4.2.2}%
\contentsline {subsubsection}{\numberline {3.4.2.3}data\_size}{9}{subsubsection.3.4.2.3}%
\contentsline {subsubsection}{\numberline {3.4.2.4}partition\_id}{9}{subsubsection.3.4.2.4}%
\contentsline {subsubsection}{\numberline {3.4.2.5}reader}{9}{subsubsection.3.4.2.5}%
\contentsline {subsubsection}{\numberline {3.4.2.6}version}{9}{subsubsection.3.4.2.6}%
\contentsline {subsubsection}{\numberline {3.4.2.7}writer}{9}{subsubsection.3.4.2.7}%
\contentsline {section}{\numberline {3.5}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partition\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}config\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{9}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Detailed Description}{10}{subsection.3.5.1}%
\contentsline {subsection}{\numberline {3.5.2}Field Documentation}{10}{subsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.2.1}base\_address}{10}{subsubsection.3.5.2.1}%
\contentsline {subsubsection}{\numberline {3.5.2.2}id}{10}{subsubsection.3.5.2.2}%
\contentsline {subsubsection}{\numberline {3.5.2.3}size}{10}{subsubsection.3.5.2.3}%
\contentsline {section}{\numberline {3.6}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partition\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}metadata\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{10}{section.3.6}%
\contentsline {subsection}{\numberline {3.6.1}Detailed Description}{11}{subsection.3.6.1}%
\contentsline {subsection}{\numberline {3.6.2}Field Documentation}{11}{subsection.3.6.2}%
\contentsline {subsubsection}{\numberline {3.6.2.1}crc}{11}{subsubsection.3.6.2.1}%
\contentsline {subsubsection}{\numberline {3.6.2.2}records}{11}{subsubsection.3.6.2.2}%
\contentsline {subsubsection}{\numberline {3.6.2.3}version}{11}{subsubsection.3.6.2.3}%
\contentsline {chapter}{\numberline {4}File Documentation}{13}{chapter.4}%
\contentsline {section}{\numberline {4.1}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}block\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}device.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c}{13}{section.4.1}%
\contentsline {section}{\numberline {4.2}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}block\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}device.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{13}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{14}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Enumeration Type Documentation}{14}{subsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.2.1}dsf\_bd\_err\_t}{14}{subsubsection.4.2.2.1}%
\contentsline {subsection}{\numberline {4.2.3}Function Documentation}{14}{subsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.3.1}dsf\_bd\_deinit()}{15}{subsubsection.4.2.3.1}%
\contentsline {subsubsection}{\numberline {4.2.3.2}dsf\_bd\_get\_size()}{15}{subsubsection.4.2.3.2}%
\contentsline {subsubsection}{\numberline {4.2.3.3}dsf\_bd\_init()}{15}{subsubsection.4.2.3.3}%
\contentsline {subsubsection}{\numberline {4.2.3.4}dsf\_bd\_read()}{15}{subsubsection.4.2.3.4}%
\contentsline {subsubsection}{\numberline {4.2.3.5}dsf\_bd\_write()}{16}{subsubsection.4.2.3.5}%
\contentsline {section}{\numberline {4.3}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}block\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}device.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h}{16}{section.4.3}%
\contentsline {section}{\numberline {4.4}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c File Reference}{17}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Detailed Description}{17}{subsection.4.4.1}%
\contentsline {section}{\numberline {4.5}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c}{18}{section.4.5}%
\contentsline {section}{\numberline {4.6}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{18}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Detailed Description}{19}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}Typedef Documentation}{19}{subsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.2.1}dsf\_logger\_id\_t}{20}{subsubsection.4.6.2.1}%
\contentsline {subsection}{\numberline {4.6.3}Enumeration Type Documentation}{20}{subsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.3.1}dsf\_logger\_err\_t}{20}{subsubsection.4.6.3.1}%
\contentsline {subsection}{\numberline {4.6.4}Function Documentation}{20}{subsection.4.6.4}%
\contentsline {subsubsection}{\numberline {4.6.4.1}dsf\_logger\_count()}{20}{subsubsection.4.6.4.1}%
\contentsline {subsubsection}{\numberline {4.6.4.2}dsf\_logger\_create()}{21}{subsubsection.4.6.4.2}%
\contentsline {subsubsection}{\numberline {4.6.4.3}dsf\_logger\_read()}{22}{subsubsection.4.6.4.3}%
\contentsline {subsubsection}{\numberline {4.6.4.4}dsf\_logger\_reset()}{22}{subsubsection.4.6.4.4}%
\contentsline {subsubsection}{\numberline {4.6.4.5}dsf\_logger\_sync()}{23}{subsubsection.4.6.4.5}%
\contentsline {subsubsection}{\numberline {4.6.4.6}dsf\_logger\_write()}{23}{subsubsection.4.6.4.6}%
\contentsline {section}{\numberline {4.7}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h}{24}{section.4.7}%
\contentsline {section}{\numberline {4.8}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partitioner.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c File Reference}{24}{section.4.8}%
\contentsline {subsection}{\numberline {4.8.1}Detailed Description}{25}{subsection.4.8.1}%
\contentsline {subsection}{\numberline {4.8.2}Macro Definition Documentation}{25}{subsection.4.8.2}%
\contentsline {subsubsection}{\numberline {4.8.2.1}DSF\_MAX\_JOURNAL\_RECORD}{26}{subsubsection.4.8.2.1}%
\contentsline {subsubsection}{\numberline {4.8.2.2}DSF\_MAX\_PARTITION}{26}{subsubsection.4.8.2.2}%
\contentsline {subsection}{\numberline {4.8.3}Typedef Documentation}{26}{subsection.4.8.3}%
\contentsline {subsubsection}{\numberline {4.8.3.1}dsf\_partition\_record\_t}{26}{subsubsection.4.8.3.1}%
\contentsline {section}{\numberline {4.9}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partitioner.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c}{26}{section.4.9}%
\contentsline {section}{\numberline {4.10}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partitioner.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{27}{section.4.10}%
\contentsline {subsection}{\numberline {4.10.1}Detailed Description}{28}{subsection.4.10.1}%
\contentsline {subsection}{\numberline {4.10.2}Typedef Documentation}{28}{subsection.4.10.2}%
\contentsline {subsubsection}{\numberline {4.10.2.1}dsf\_partition\_id\_t}{28}{subsubsection.4.10.2.1}%
\contentsline {subsection}{\numberline {4.10.3}Enumeration Type Documentation}{28}{subsection.4.10.3}%
\contentsline {subsubsection}{\numberline {4.10.3.1}dsf\_partition\_err\_t}{29}{subsubsection.4.10.3.1}%
\contentsline {subsection}{\numberline {4.10.4}Function Documentation}{29}{subsection.4.10.4}%
\contentsline {subsubsection}{\numberline {4.10.4.1}dsf\_journal\_start()}{29}{subsubsection.4.10.4.1}%
\contentsline {subsubsection}{\numberline {4.10.4.2}dsf\_journal\_stop()}{29}{subsubsection.4.10.4.2}%
\contentsline {subsubsection}{\numberline {4.10.4.3}dsf\_partition\_create()}{30}{subsubsection.4.10.4.3}%
\contentsline {subsubsection}{\numberline {4.10.4.4}dsf\_partition\_format()}{30}{subsubsection.4.10.4.4}%
\contentsline {subsubsection}{\numberline {4.10.4.5}dsf\_partition\_init()}{31}{subsubsection.4.10.4.5}%
\contentsline {subsubsection}{\numberline {4.10.4.6}dsf\_partition\_read()}{31}{subsubsection.4.10.4.6}%
\contentsline {subsubsection}{\numberline {4.10.4.7}dsf\_partition\_write()}{31}{subsubsection.4.10.4.7}%
\contentsline {section}{\numberline {4.11}dsf\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partitioner.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h}{32}{section.4.11}%
\contentsline {chapter}{\numberline {5}Example Documentation}{33}{chapter.5}%
\contentsline {section}{\numberline {5.1}example\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}logger.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c}{33}{section.5.1}%
\contentsline {section}{\numberline {5.2}example\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}partitioner.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}c}{34}{section.5.2}%
\contentsline {chapter}{Index}{35}{section*.21}%
