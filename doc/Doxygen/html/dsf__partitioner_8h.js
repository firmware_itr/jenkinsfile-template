var dsf__partitioner_8h =
[
    [ "dsf_partition_config_t", "structdsf__partition__config__t.html", null ],
    [ "dsf_partition_id_t", "dsf__partitioner_8h.html#a594df67234bc79ce4b6e62879a381b6c", null ],
    [ "dsf_partition_err_t", "dsf__partitioner_8h.html#a2dd5cb943e6775f2a265393e441783cf", [
      [ "DSF_PARTITION_OK", "dsf__partitioner_8h.html#a2dd5cb943e6775f2a265393e441783cfadd06a5499f2934efd14ff2f9e1423790", null ],
      [ "DSF_PARTITION_ERR", "dsf__partitioner_8h.html#a2dd5cb943e6775f2a265393e441783cfa543a540a973a82bec7d79b13be11229b", null ]
    ] ],
    [ "dsf_journal_start", "dsf__partitioner_8h.html#a9b7a1e21caa0706c7316b65eb88dc4b8", null ],
    [ "dsf_journal_stop", "dsf__partitioner_8h.html#a845719ea1a2b0984e1e3a6111aaebc31", null ],
    [ "dsf_partition_create", "dsf__partitioner_8h.html#a383c0d019b17495917cae910d57bd57f", null ],
    [ "dsf_partition_format", "dsf__partitioner_8h.html#a7773b313ed9ae5f996078d6f83c006f1", null ],
    [ "dsf_partition_init", "dsf__partitioner_8h.html#af981bb43f617ed81750bb71bf3e98646", null ],
    [ "dsf_partition_read", "dsf__partitioner_8h.html#a77a7a8dc7fe549b1b741fc619cca9f2c", null ],
    [ "dsf_partition_write", "dsf__partitioner_8h.html#a35cd1eb938a7045ee8260a4792d6ed17", null ]
];