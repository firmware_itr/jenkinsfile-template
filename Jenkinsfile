pipeline {
  agent any 
  
  environment {
    // Email of recipients. EX: EMAIL_USER = 'abc@itrvn.com, def@itrvn.com'
    EMAIL_USER = 'hieudoan@itrvn.com'
  }

  stages {
    stage('Run Tests') {
      steps {
        // Run test
        powershell  '''
          echo workspace_path: "$env:WORKSPACE"
          cd $env:WORKSPACE
          & ceedling test:all
          & ceedling gcov:all
          & ceedling utils:gcov
        '''

        publishHTML (target: [
          allowMissing: false,
          alwaysLinkToLastBuild: true,
          keepAll: true,
          reportDir: './ceedling/build/artifacts/gcov',
          reportFiles: 'GcovCoverageResults.html',
          reportName: 'RCov Report'
        ])

        // Report unit test
        junit 'ceedling/build/artifacts/test/report.xml'
      }
    }

    stage('Static code analysis') {
      steps {
        bat  '''
          echo "Cppcheck static analyze"
          cppcheck --enable=all --inconclusive --xml --xml-version=2 %WORKSPACE%/master %WORKSPACE%/slave \
          %WORKSPACE%/Src 2> cppcheck.xml
        '''
        publishCppcheck displayErrorSeverity: true, displayNoCategorySeverity: true, displayPerformanceSeverity: true, displayPortabilitySeverity: true, displayStyleSeverity: true, displayWarningSeverity: true, healthy: '10', pattern: 'cppcheck.xml', unHealthy: '100'
      }
    }

    stage('Notify build status') {
      steps {
        echo 'Notify build status'
        updateGitlabCommitStatus name: 'build', state: 'pending'
        updateGitlabCommitStatus name: 'build', state: 'success'
      }
    }
  }

  // Notification the report to recipients via email
  post {
    always  {
      // Copy email template to path default
      powershell  '''
        echo 'Sending email...'
        cp $env:WORKSPACE/email.template $env:JENKINS_HOME/email-templates/email1.template
      '''
      
      // References global variable: http://115.79.33.14:1002/job/jenkinsfile_template/pipeline-syntax/globals
      emailext (
        to: "${EMAIL_USER}",
        subject: "[XII Jenkins] ${currentBuild.currentResult}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: '${SCRIPT, template="email1.template"} ${FILE, path="ceedling/build/artifacts/gcov/GcovCoverageResults.html"}',
        mimeType:'text/html'
      )
    }
  }
}
