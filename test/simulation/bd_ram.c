/**
 * @file       bd_ram.c
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      Block Device on RAM
 * @note       None
 */
/* Public includes ---------------------------------------------------------- */
#include "bd_ram.h"

/* Private includes --------------------------------------------------------- */
/* Private defines ---------------------------------------------------------- */
#define assert_param(expr)    do { if (!(expr)) return DSF_BD_ERR; } while (0)

/* Private enumerate/structure ---------------------------------------------- */
/* Private macros ----------------------------------------------------------- */
/* Public variables --------------------------------------------------------- */
/* Private variables -------------------------------------------------------- */
static uint8_t ram[BD_RAM_SIZE] = { 0 };

/* Private prototypes ------------------------------------------------------- */
/* Public implementations --------------------------------------------------- */
dsf_bd_err_t bd_ram_init(void)
{
	return DSF_BD_OK;
}

dsf_bd_err_t bd_ram_deinit(void)
{
	return DSF_BD_OK;
}

dsf_bd_err_t bd_ram_read(uint32_t address, uint8_t *buffer, size_t length)
{
	assert_param((address + length) <= BD_RAM_SIZE);
	assert_param(buffer != NULL);

	memcpy(buffer, &ram[address], length);

	return DSF_BD_OK;
}

dsf_bd_err_t bd_ram_write(uint32_t address, uint8_t *buffer, size_t length)
{
	assert_param((address + length) <= BD_RAM_SIZE);
	assert_param(buffer != NULL);

	memcpy(&ram[address], buffer, length);

	return DSF_BD_OK;
}

size_t bd_ram_get_size(void)
{
	return BD_RAM_SIZE;
}

/* Private implementations -------------------------------------------------- */


/* End of file -------------------------------------------------------------- */
