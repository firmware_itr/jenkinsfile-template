/**
 * @file       bd_ram.h
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      Block Device on RAM
 * @note       None
 * @example    None
 */
/* Define to prevent recursive inclusion ------------------------------------ */
#ifndef __BD_RAM_H
#define __BD_RAM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ----------------------------------------------------------------- */
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include "dsf_block_device.h"

/* Public defines ----------------------------------------------------------- */
#define BD_RAM_SIZE  (1024 * 1024)

/* Public enumerate/structure ----------------------------------------------- */
/* Public macros ------------------------------------------------------------ */
/* Public variables --------------------------------------------------------- */
/* Public APIs -------------------------------------------------------------- */
dsf_bd_err_t bd_ram_init(void);
dsf_bd_err_t bd_ram_deinit(void);
dsf_bd_err_t bd_ram_read(uint32_t address, uint8_t *buffer, size_t length);
dsf_bd_err_t bd_ram_write(uint32_t address, uint8_t *buffer, size_t length);
size_t bd_ram_get_size(void);

/* -------------------------------------------------------------------------- */
#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* __BD_RAM_H */

/* End of file -------------------------------------------------------------- */
