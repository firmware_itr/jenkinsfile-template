#include "unity.h"
#include "bd_ram.h"

/* Test Setup --------------------------------------------------------------- */
void setUp(void)
{
}

void tearDown(void)
{
}

/* Test Cases --------------------------------------------------------------- */
void test_bd_ram_get_size(void)
{
	TEST_ASSERT_EQUAL(BD_RAM_SIZE, bd_ram_get_size()); 
}

void test_bd_ram_rw(void)
{
	uint32_t address = 0;
	static uint8_t w_buf[BD_RAM_SIZE];
	static uint8_t r_buf[BD_RAM_SIZE] = { 0 };

	memset(w_buf, 0xA5, sizeof(w_buf));

	TEST_ASSERT_EQUAL(0, bd_ram_write(address, w_buf, sizeof(w_buf)));
	TEST_ASSERT_EQUAL(0, bd_ram_read(address, r_buf, sizeof(r_buf)));
	TEST_ASSERT_EQUAL(0, memcmp(w_buf, r_buf, sizeof(w_buf)));
}
