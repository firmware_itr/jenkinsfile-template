#include "unity.h"
#include "dsf_block_device.h"
#include "bd_ram.h"

/* Test Setup --------------------------------------------------------------- */
void setUp(void)
{
	dsf_block_device_t ram_dev = {
		.init   = bd_ram_init,
		.deinit = bd_ram_deinit,
		.read   = bd_ram_read,
		.write  = bd_ram_write,
		.size   = bd_ram_get_size
	};
	dsf_bd_create(&ram_dev);
}

void tearDown(void)
{
}

/* Test Cases --------------------------------------------------------------- */
void test_dsf_bd_init(void)
{
	TEST_ASSERT_EQUAL(DSF_BD_OK, dsf_bd_init());
}

void test_dsf_bd_deinit(void)
{
	TEST_ASSERT_EQUAL(DSF_BD_OK, dsf_bd_deinit());	
}

void test_dsf_bd_readwrite(void)
{
	uint32_t address = 0;
	static uint8_t w_buf[BD_RAM_SIZE];
	static uint8_t r_buf[BD_RAM_SIZE];

	memset(w_buf, 0xA5, sizeof(w_buf));
	dsf_bd_write(address, w_buf, sizeof(w_buf));

	TEST_ASSERT_EQUAL(DSF_BD_OK, dsf_bd_read(address, r_buf, sizeof(r_buf)));
	TEST_ASSERT_EQUAL(0, memcmp(w_buf, r_buf, sizeof(w_buf)));
}

void test_dsf_bd_size(void)
{
	TEST_ASSERT_EQUAL(BD_RAM_SIZE, dsf_bd_get_size());
}