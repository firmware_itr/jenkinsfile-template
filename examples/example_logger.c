/**
 * @file       example_logger.c
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      
 * @note       None
 */
/* Public includes ---------------------------------------------------------- */
#include "dsf_logger.h"
#include <stdio.h>

#define CHECK(expr) \
	do { \
		if ((expr) != DSF_LOGGER_OK) { \
			printf("ERR: %s:%d\n", __FILE__, __LINE__); \
			return -1; \
		} \
	} while (0)

#define LOGGER_ID		0xA5A5A5A5

typedef struct
{
	uint8_t data_1;
	uint8_t data_2;
}
data_t;

/* Entry -------------------------------------------------------------------- */
int main(void)
{
	dsf_logger_config_t config = {
		.id        = LOGGER_ID,
		.data_size = sizeof(data_t),
		.capacity  = 5
	};
	dsf_logger_err_t err = DSF_LOGGER_OK;

	// Create a logger
	err = dsf_logger_create(&config, 1);
	CHECK(err);

	// Clear logger data
	err = dsf_logger_reset(LOGGER_ID);
	CHECK(err);

	// Write data to logger
	data_t w_buf = {
		.data_1 = 0xAA,
		.data_2 = 0xFF
	};
	err = dsf_logger_write(LOGGER_ID, &w_buf, 1);
	CHECK(err);

	// Get the number of unread items in a logger
	size_t counter = 0;
	err = dsf_logger_count(LOGGER_ID, &counter);
	printf("Number of unread items: %lu\n", counter);
	CHECK(err);

	// Read data from logger
	data_t r_buf;
	err = dsf_logger_read(LOGGER_ID, &r_buf, 1);
	CHECK(err);

	// Get the number of unread items in a logger
	err = dsf_logger_count(LOGGER_ID, &counter);
	printf("Number of unread items: %lu\n", counter);

	return 0;
}


/* End of file -------------------------------------------------------------- */
