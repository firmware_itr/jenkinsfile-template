/**
 * @file       example_partitioner.c
 * @copyright  
 * @license    
 * @version    1.0.0
 * @date       2021
 * @author     
 * @brief      
 * @note       None
 */
/* Public includes ---------------------------------------------------------- */
#include "dsf_partitioner.h"
#include <stdio.h>

#define CHECK(expr) \
	do { \
		if ((expr) != DSF_PARTITION_OK) { \
			printf("ERR: %s:%d\n", __FILE__, __LINE__); \
			return -1; \
		} \
	} while (0)

#define PARTITION_ID_1		0xA5A5A5A5
#define PARTITION_ID_2		0xFEFEFAFA

/* Entry -------------------------------------------------------------------- */
int main(void)
{
	dsf_partition_err_t err = DSF_PARTITION_OK;

	// Load Partition Metadata and check unsynced data in journal
	err = dsf_partition_init();
	CHECK(err);

	// Format: reset Partition Metadata to default values
	err = dsf_partition_format();
	CHECK(err);

	// Create partitions
	dsf_partition_create_cfg_t config[] = {
		{
			.id           = PARTITION_ID_1,
			.base_address = 0x4000,
			.size         = 1024,
		},
		{
			.id           = PARTITION_ID_2,
			.base_address = 0x4400,
			.size         = 2048,
		}
	};
	err = dsf_partition_create(&config, sizeof(config) / sizeof(dsf_partition_create_cfg_t));
	CHECK(err);

	uint8_t w_buf[] = "hello";

	// Write data to partition without journaling
	err = dsf_partition_write(PARTITION_ID_1, 0, w_buf, sizeof(w_buf) - 1);
	CHECK(err);

	// Write data to partition with journaling -----------------------------------
	err = dsf_journal_start();
	CHECK(err);

	err = dsf_partition_write(PARTITION_ID_1, 128, w_buf, sizeof(w_buf) - 1);
	CHECK(err);
	err = dsf_partition_write(PARTITION_ID_2, 512, w_buf, sizeof(w_buf) - 1);
	CHECK(err);

	err = dsf_journal_stop();
	CHECK(err);
	// ---------------------------------------------------------------------------

	// Read data from partition
	uint8_t r_buf[16] = { 0 };
	err = dsf_partition_read(PARTITION_ID_1, r_buf, sizeof(w_buf) - 1);
	CHECK(err);


	return 0;
}

/* End of file -------------------------------------------------------------- */
